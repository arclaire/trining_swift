//
//  RootVC.swift
//  trining_swift
//
//  Created by Lucy on 3/24/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit

enum stateRoot {
  case stateDefault
  case stateLeftExpanded
  case stateRightExpanded
}

extension UIStoryboard {
  static func getMainStoryboard() -> UIStoryboard {
    return UIStoryboard.init(name: "Main", bundle: nil)
  }
}
class RootVC: UIViewController {

  @IBOutlet weak var vwParent: UIView!
 
  var vcRootTabBar: RootTabBarVC!

  var vcRootLeft: RootLeftVC?
  var ncRoot:RootNC?
  var nbRoot:RootNB?
  
  var stateCurrent: stateRoot = .stateDefault {
    didSet {
      let isAddShadow = stateCurrent != .stateDefault
      self.addShadow(isAddShadow)
    }
  }
  let floatCenterOffset: CGFloat = 60
  
  
  //MARK: - LIFE CYCLE
  override func viewDidLoad() {
    super.viewDidLoad()
    let sbMain = UIStoryboard.getMainStoryboard()
    self.vcRootTabBar = sbMain.instantiateViewControllerWithIdentifier(ID_TAB_BAR_ROOT) as? RootTabBarVC
    self.vcRootTabBar.didMoveToParentViewController(self)
    self.vcRootTabBar.selectedIndex = 0

    self.addChildViewController(self.vcRootTabBar)
    //self.presentViewController(self.vcRootTabBar, animated: false, completion: nil)
   self.view.addSubview(self.vcRootTabBar.view!)
    
    self.ncRoot = self.vcRootTabBar.viewControllers![0] as? RootNC
   // self.ncRoot?.vwRootNB.delegateNB = self
  }

  override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
      // Dispose of any resources that can be recreated.
    
  }

 //MARK: - UI METHODS
  func addShadow(isAddShadow: Bool) {
    if (isAddShadow) {
      self.vcRootTabBar.view.layer.shadowOpacity = 0.8
    } else {
      self.vcRootTabBar.view.layer.shadowOpacity = 0.0
    }
  }

  func addLeftPanelViewController() {
    
    if (vcRootLeft == nil) {
      let sbMain = UIStoryboard.getMainStoryboard()
      self.vcRootLeft = sbMain.instantiateViewControllerWithIdentifier(ID_VC_LEFT_ROOT) as? RootLeftVC
      self.addChildLeftVC(self.vcRootLeft!)
    }
  }
  
  func addChildLeftVC(vcLeft: RootLeftVC) {
    view.insertSubview(vcLeft.view, atIndex: 0)
    
    addChildViewController(vcLeft)
    vcLeft.didMoveToParentViewController(self)
  }
  
  func animateLeftPanel(isExpanded: Bool) {
    if (isExpanded) {
      stateCurrent = .stateLeftExpanded
      
      animateCenterPanelXPosition(targetPosition: CGRectGetWidth(self.vcRootTabBar.view.frame) - floatCenterOffset)
    } else {
      animateCenterPanelXPosition(targetPosition: 0) { finished in
        self.stateCurrent = .stateDefault
        
        self.vcRootLeft!.view.removeFromSuperview()
        self.vcRootLeft = nil;
      }
    }
  }
  
  
  func animateCenterPanelXPosition(targetPosition targetPosition: CGFloat, completion: ((Bool) -> Void)! = nil) {
    UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .CurveEaseInOut, animations: {
      self.vcRootTabBar.view.frame.origin.x = targetPosition
      }, completion: completion)
    
  }
  
}

extension RootVC:RootNBDelegate {
  
  func toggleLeftPanel() {
    let isNotExpanded = (stateCurrent != .stateLeftExpanded)
    
    if isNotExpanded {
      self.addLeftPanelViewController()
    }
    
    self.animateLeftPanel(isNotExpanded)
  }
}

