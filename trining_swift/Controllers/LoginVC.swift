//
//  LoginVC.swift
//  trining_swift
//
//  Created by Lucy on 3/26/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class LoginVC: UIViewController {

  
  func doLoginFB() {
    let loginFb: FBSDKLoginManager = FBSDKLoginManager()
    loginFb.loginBehavior = .SystemAccount
    loginFb.logOut()
    
    let strFBPermission = ["email"]
    
    loginFb.logInWithReadPermissions(strFBPermission, fromViewController: self, handler: { (result:FBSDKLoginManagerLoginResult!, error:NSError!) -> Void in
      if error != nil {
        loginFb.logOut()
        let strError:String = String(error.localizedDescription)
        UIAlertView(title: "ERROR", message: strError, delegate: nil, cancelButtonTitle: "Ok").show()
        
      } else if result.isCancelled {
        loginFb.logOut()
      } else {
        
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"name, email,first_name,location,last_name,locale,hometown"])
        graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
          
          if error != nil  {
            loginFb.logOut()
            let strError:String = String(error.localizedDescription)
            UIAlertView(title: "ERROR", message: strError, delegate: nil, cancelButtonTitle: "Ok").show()
          } else {
            
            if let dictResult:NSDictionary = result as? NSDictionary {
              debugPrint(dictResult)
            } else {
              loginFb.logOut()
              
            }
          }
        })
      }
    })

  }
}
