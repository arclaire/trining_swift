//
//  ExtView.swift
//  trining_swift
//
//  Created by Lucy on 4/24/16.
//  Copyright © 2016 cy. All rights reserved.
//

import Foundation

import UIKit

extension UIView {
 
   func loadViewFromNIB() -> UIView {
    let bundle = NSBundle(forClass:self.dynamicType)
    let nib = UINib(nibName: String(self.classForCoder), bundle: bundle)
    let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
    
    return view
  }
}