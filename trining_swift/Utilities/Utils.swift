//
//  Utils.swift
//  trining_swift
//
//  Created by Lucy on 5/9/16.
//  Copyright © 2016 cy. All rights reserved.
//


import Foundation
import UIKit
import AVFoundation

class Utils: NSObject {

  static func getCurrencySymbol(CountryCode strCode:String) -> String {
    let strCurrencySymbol:String = "$"
    let identifiers : NSArray = NSLocale.availableLocaleIdentifiers()
    var localID: NSLocale = NSLocale.init(localeIdentifier :  "en_US")
    
    //let englishLocale : NSLocale = NSLocale.init(localeIdentifier :  "en_US")
    for anyLocaleID in identifiers {
      let anyLocale : NSLocale  = NSLocale.init(localeIdentifier : anyLocaleID as! String)
      //var symbol : String? = anyLocale.objectForKey(NSLocaleCurrencySymbol) as? String
      //let name:String? =  englishLocale.displayNameForKey(NSLocaleIdentifier, value: anyLocaleID)//locale.displayNameForKey(NSLocaleIdentifier, value: anyLocaleID)!
      //debugPrint("name",name)
      if let stringCountryCode : String = anyLocale.objectForKey(NSLocaleCountryCode) as? String {
        if strCode == stringCountryCode {
          localID = anyLocale
          break
        }
      }
    }
    
    if let _strCurrencySymbol:String = (localID.objectForKey(NSLocaleCurrencySymbol) as? String)! {
      return _strCurrencySymbol
    } else {
      return strCurrencySymbol
    }
  }
  
  static func setLocalId(withCountryCode strCode:String) {
    var localID:String = "en_SG"
    let identifiers : NSArray = NSLocale.availableLocaleIdentifiers()
    //let englishLocale : NSLocale = NSLocale.init(localeIdentifier :  "en_US")
    for anyLocaleID in identifiers {
      let anyLocale : NSLocale  = NSLocale.init(localeIdentifier : anyLocaleID as! String)
      //var symbol : String? = anyLocale.objectForKey(NSLocaleCurrencySymbol) as? String
      //let name:String? =  englishLocale.displayNameForKey(NSLocaleIdentifier, value: anyLocaleID)//locale.displayNameForKey(NSLocaleIdentifier, value: anyLocaleID)!
      //debugPrint("name",name)
      if let stringCountryCode : String = anyLocale.objectForKey(NSLocaleCountryCode) as? String {
        if strCode == stringCountryCode {
          localID = anyLocaleID as! String
          break
        }
      }
    }
    
    NSUserDefaults.standardUserDefaults().setValue(localID, forKey:"SAMPLE" ) //Constants.PREFERENCE_STR_LOCAL_ID
    
  }
  
  static func formatToDecimalString(strNumber:String, withCountryCode strCode:String) -> String {
    var localID:String = "en_SG"
    if NSUserDefaults.standardUserDefaults().objectForKey("SAMPLE") != nil {
      localID = NSUserDefaults.standardUserDefaults().objectForKey("SAMPLE") as! String
    }
    
    let floatCurrency:Float = NSString(string: strNumber).floatValue
    let formatter = NSNumberFormatter()
    formatter.numberStyle = NSNumberFormatterStyle.CurrencyStyle
    formatter.maximumFractionDigits = 0
    formatter.locale = NSLocale(localeIdentifier:localID)
    let number:NSNumber = NSNumber(float: floatCurrency)
    
    return formatter.stringFromNumber(number)!
  }
  
  static func getCurrencySymbol2(CountryCode strCode:String) -> String {
    let strCurrencySymbol:String = "$"
    
    var localID: String = "en_US"
    if NSUserDefaults.standardUserDefaults().objectForKey("SAMPLE") != nil {
      localID = NSUserDefaults.standardUserDefaults().objectForKey("SAMPLE") as! String
    }
    
    if let _strCurrencySymbol:String = (NSLocale(localeIdentifier: localID).objectForKey(NSLocaleCurrencySymbol) as? String)! {
      return _strCurrencySymbol
    } else {
      return strCurrencySymbol
    }
  }

  let words = ["Cat", "Chicken", "fish", "Dog",
               "Mouse", "Guinea Pig", "monkey"]
  
  typealias Entry = (Character, [String])
  
  func buildIndex(words: [String]) -> [Entry] {
    var result = [Entry]()
    
    var letters = [Character]()
    
//    let sections = messages.map { // when using model
//      $0.strCreated
//    }
    for word in words {
      let firstLetter = Character(word.substringToIndex(word.startIndex.advancedBy(1)).uppercaseString)
      
      if !letters.contains(firstLetter) {
        letters.append(firstLetter)
      }
    }
    
    for letter in letters {
      var wordsForLetter = [String]()
      for word in words {
        let firstLetter = Character(word.substringToIndex(word.startIndex.advancedBy(1)).uppercaseString)
        
        if firstLetter == letter {
          wordsForLetter.append(word)
        }
      }
      result.append((letter, wordsForLetter))
    }
    return result
  }
  
  func buildIndex2(words: [String]) -> [Entry] {
    //let letters = words.map {
    //  (word) -> Character in
    //  Character(word.substringToIndex(word.startIndex.advancedBy(1)).uppercaseString)
    //}
    
    func firstLetter(str: String) -> Character {
      return Character(str.substringToIndex(str.startIndex.advancedBy(1)).uppercaseString)
    }
    
    let letters = words.map {
      (word) -> Character in
      firstLetter(word)
    }
    
    let distinctLetters = self.distinct(letters)
    debugPrint(distinctLetters)
    
    return distinct(words.map(firstLetter))
      .map {
        (letter) -> Entry in
        return (letter, words.filter {
          (word) -> Bool in
          firstLetter(word) == letter
          })
    }
    
    //return distinctLetters.map {
    //  (letter) -> Entry in
    //  return (letter, words.filter {
    //    (word) -> Bool in
    //    Character(word.substringToIndex(word.startIndex.advancedBy(1)).uppercaseString) == letter
    //    })
    //}
    //return distinctLetters.map {
    //  (letter) -> Entry in
    //  return (letter, [])
    //}
    //return [Entry]()
  }
  
  func distinct<T: Equatable>(source: [T]) -> [T] {
    var unique = [T]()
    for item in source {
      if !unique.contains(item) {
        unique.append(item)
      }
    }
    return unique
  }

}
