//
//  ConversationView.swift
//  Closet
//
//  Created by Lucy on 4/20/16.
//  Copyright © 2016 Reebonz. All rights reserved.
//

import UIKit

protocol DelegateConversationView:NSObjectProtocol {
  func delegateGoback()
  //func delegateDoSendChat(dataConversationMsg:ConversationMessage, strMsg:String)
  func delegateWithdraw()
  func delegateSubmitOffer(strPrice:String)
}
class ConversationView: UIView {

  //MARK: Outlet

  @IBOutlet weak var scrollView: UIScrollviewCustom!
  @IBOutlet weak private var tblChat: UITableView!
  @IBOutlet weak private var vwHeader: UIView!
  @IBOutlet weak private var vwHeaderProduct: UIView!
  
  @IBOutlet weak private var vwActionButtons: UIView!
  @IBOutlet weak private var vwActSingle: UIView!
  @IBOutlet weak private var vwActDouble: UIView!
  @IBOutlet weak private var vwContainerInput: UIView!
  @IBOutlet weak private var vwLineTop: UIView!
  @IBOutlet weak private var vwContainerDetail: UIView!
  @IBOutlet weak private var vwOtherBuyers: UIView!
  @IBOutlet weak private var vwCurrentOffer: UIView!
  @IBOutlet weak private var vwContainerParentInputOffer: UIView!
  @IBOutlet weak private var vwContainerInputOffer: UIView!
  @IBOutlet weak private var vwActOffer: UIView!
  
  @IBOutlet weak private var lblInputCounter: UILabel!
  @IBOutlet weak private var lblOtherBuyers: UILabel!
  @IBOutlet weak private var lblOtherBuyersCounter: UILabel!
  
  @IBOutlet weak private var lblCurrentOffer: UILabel!
  @IBOutlet weak private var lblCurrentOfferCounter: UILabel!
  
  @IBOutlet weak private var lblBrand: UILabel!
  @IBOutlet weak private var lblBrandTitle: UILabel!
  
  @IBOutlet weak private var lblPriceOri: UILabel!
  @IBOutlet weak private var lblPrice: UILabel!
  @IBOutlet weak private var lblSellingPrice: UILabel!
  @IBOutlet weak private var lblTitleInputOffer: UILabel!
  
  @IBOutlet weak private var imgProduct: UIImageView!
  @IBOutlet weak private var imgIconCamera: UIImageView!
  
  @IBOutlet weak private var btnActOfferLeft: UIButton!
  @IBOutlet weak private var btnActOfferRight: UIButton!
  
  @IBOutlet weak private var btnActSingle: UIButton!
  @IBOutlet weak private var btnActHalfLeft: UIButton!
  @IBOutlet weak private var btnActHalfRight: UIButton!
  @IBOutlet weak private var btnInputSend: UIButton!
  @IBOutlet weak private var txtvwChatInput: CustomTextView!
  
  @IBOutlet weak private var txtInputOfferPrice: UITextField!

  //MARK: Layouts
  
  @IBOutlet weak var consVwActionButtonsHeight: NSLayoutConstraint!
  @IBOutlet weak private var consVwContainerInputBotToSuperviewBot: NSLayoutConstraint!
  @IBOutlet weak private var consTxtChatInputLeadToVwContainerInputLead: NSLayoutConstraint!
  @IBOutlet weak private var consTxtChatInputLeadToImgCameraTrail: NSLayoutConstraint!
  @IBOutlet weak private var consTxtVwChatInputHeight: NSLayoutConstraint!
  @IBOutlet weak var consLblPriceLeadToLblPriceOriTrail: NSLayoutConstraint!
  @IBOutlet weak var consLblPriceLeadToImgProductTrail: NSLayoutConstraint!
  @IBOutlet weak var consVwContainerParentInputOfferBotToSuperviewTop: NSLayoutConstraint!
  @IBOutlet weak var consTblChatHeight: NSLayoutConstraint!
  
  //MARK: Attributes
  
  weak var delConversationView:DelegateConversationView?
  var isSeller:Bool = true
  private var vwConversation:UIView!
//  var dataConversation:Conversation = Conversation()
 var arrayOfChats:[String] = [String]() {
    didSet{
//      self.tblChat.reloadData()
      self.tableViewScrollToBottom(true)
    }
  }
  
  private var isKeyboardShow:Bool = false
  private var isKeyboardOffer:Bool = false
  private var gestureTapDismiss:UITapGestureRecognizer!
  private var intCounterSystemMsgDummy:Int = 0
  private var floatDefaultTblHeight:CGFloat = 100
  
  //MARK: Initialization
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)!
    
    self.vwConversation = self.loadViewFromNIB()
    if let _ = self.vwConversation {
      self.addSubview(self.vwConversation)
    }
  }
  
  //MARK: Lifecycle

  override func awakeFromNib() {
    super.awakeFromNib()
    //self.vwHeader.delHeaderView = self
    //self.vwHeader.setHeaderForProductDetail(withHeaderTitle: "User")
    self.prepareUI()
    self.gestureTapDismiss = UITapGestureRecognizer(target: self, action:#selector(self.dismissKeyboard))
    self.gestureTapDismiss.numberOfTapsRequired = 1
    self.addGestureRecognizer(self.gestureTapDismiss)
    self.scrollView.delegate = self
    
    //self.tblChat.registerNib(UINib(nibName:"ConversationCellLeft", bundle: nil), forCellReuseIdentifier: "ConversationCellLeft")
    //self.tblChat.registerNib(UINib(nibName:"ConversationCellRight",bundle: nil), forCellReuseIdentifier: "ConversationCellRight")
    //self.tblChat.registerNib(UINib(nibName:"ConversationCellSystem",bundle: nil), forCellReuseIdentifier: "ConversationCellSystem")
    //self.tblChat.registerClass(UITableViewCell.self, forCellReuseIdentifier: "DefaultCell")
//     self.consTblChatHeight.constant = self.frame.size.height - self.vwHeaderProduct.frame.size.height - self.vwContainerInput.frame.size.height - self.vwActionButtons.frame.size.height - self.vwHeaderProduct.frame.size.height
    self.consTblChatHeight.active = false
  }
  
  //MARK: - UI Methods

  override func layoutSubviews() {
    super.layoutSubviews()
   
    //debugPrint("consHeight", self.consTblChatHeight.constant)
  }
  
  func prepareUI() {
    
    self.consVwContainerParentInputOfferBotToSuperviewTop.constant = self.frame.size.height
    self.tblChat.registerClass(UITableViewCell.self, forCellReuseIdentifier: "DefaultCell")
    //self.vwHeaderProduct.backgroundColor = Constants.COLOR_WHITE
    //self.vwContainerDetail.backgroundColor = Utils.setHexColor("AAAAAA")
    //self.vwCurrentOffer.backgroundColor = Constants.COLOR_WHITE
    //self.vwOtherBuyers.backgroundColor = Constants.COLOR_WHITE
    //self.vwContainerInput.backgroundColor = Constants.COLOR_WHITE
    self.vwContainerParentInputOffer.hidden = true
    
    //self.tblChat.backgroundColor = Utils.setHexColor("F5F5F5")
    //self.scrollView.backgroundColor = Utils.setHexColor("F5F5F5")
    
    //self.vwOtherBuyers.layer.borderColor = Utils.setHexColor("AAAAAA").CGColor
    self.vwOtherBuyers.layer.borderWidth = 0.5
    //self.vwCurrentOffer.layer.borderColor = Utils.setHexColor("AAAAAA").CGColor
    self.vwCurrentOffer.layer.borderWidth = 0.5
    //self.vwContainerParentInputOffer.backgroundColor = Constants.COLOR_BLACK_TRANSPARANT
    //self.vwContainerInputOffer.backgroundColor = Constants.COLOR_WHITE
    //self.vwActOffer.layer.borderColor = Utils.setHexColor("AAAAAA").CGColor
    self.vwActOffer.layer.borderWidth = 0.5
    //self.vwActDouble.layer.borderColor = Utils.setHexColor("AAAAAA").CGColor
    self.vwActDouble.layer.borderWidth = 0.5
    //self.vwActSingle.layer.borderColor = Utils.setHexColor("AAAAAA").CGColor
    self.vwActSingle.layer.borderWidth = 0.5
    
//    self.lblBrand.backgroundColor = Constants.COLOR_TRANSPARENT
//    self.lblBrandTitle.backgroundColor = Constants.COLOR_TRANSPARENT
//    self.lblPrice.backgroundColor = Constants.COLOR_TRANSPARENT
//    self.lblPriceOri.backgroundColor = Constants.COLOR_TRANSPARENT
//    self.lblOtherBuyers.backgroundColor = Constants.COLOR_TRANSPARENT
//    self.lblCurrentOffer.backgroundColor = Constants.COLOR_TRANSPARENT
//    self.lblInputCounter.backgroundColor = Constants.COLOR_TRANSPARENT
//    self.lblOtherBuyersCounter.backgroundColor = Constants.COLOR_TRANSPARENT
//    self.lblCurrentOfferCounter.backgroundColor = Constants.COLOR_TRANSPARENT
//    self.txtvwChatInput.backgroundColor = Constants.COLOR_TRANSPARENT  //UIColor.redColor()//
    
    //let attrStringTitle = NSMutableAttributedString(string: self.lblBrand.text!, attributes: [NSForegroundColorAttributeName: Utils.setHexColor("333333"), NSFontAttributeName : Constants.FONT_LATO_HEAVY_13!, NSKernAttributeName: 0.65])
    //self.lblBrand.attributedText = attrStringTitle
    
//    self.lblBrandTitle.textColor = Utils.setHexColor("333333")
//    self.lblPriceOri.textColor = Utils.setHexColor("888888")
//    self.lblPrice.textColor = Constants.COLOR_PINK
//    self.lblCurrentOfferCounter.textColor = Utils.setHexColor("333333")
//    self.lblCurrentOffer.textColor = Utils.setHexColor("333333")
//    self.lblOtherBuyers.textColor = Utils.setHexColor("333333")
//    self.lblOtherBuyersCounter.textColor = Utils.setHexColor("333333")
//    self.lblInputCounter.textColor = Utils.setHexColor("888888")
    
//    self.lblBrandTitle.font = Constants.FONT_LATO_HEAVY_13
//    self.lblPriceOri.font = Constants.FONT_LATO_REGULAR_13
//    self.lblPrice.font = Constants.FONT_LATO_HEAVY_15
//    self.lblCurrentOfferCounter.font = Constants.FONT_LATO_HEAVY_15
//    self.lblCurrentOffer.font = Constants.FONT_LATO_REGULAR_13
//    self.lblOtherBuyers.font = Constants.FONT_LATO_REGULAR_13
//    self.lblOtherBuyersCounter.font = Constants.FONT_LATO_HEAVY_15
//    self.lblInputCounter.font = Constants.FONT_LATO_HEAVY_15
    
//    self.btnActSingle.titleLabel?.font = Constants.FONT_LATO_HEAVY_15
//    self.btnActHalfLeft.titleLabel?.font = Constants.FONT_LATO_HEAVY_15
//    self.btnActHalfRight.titleLabel?.font = Constants.FONT_LATO_HEAVY_15
//    self.btnInputSend.titleLabel?.font = Constants.FONT_LATO_HEAVY_15
//    self.btnActOfferLeft.titleLabel?.font = Constants.FONT_LATO_HEAVY_15
//    self.btnActOfferRight.titleLabel?.font = Constants.FONT_LATO_HEAVY_15
    
//    self.btnActSingle.backgroundColor = Constants.COLOR_PINK
//    self.btnActHalfLeft.backgroundColor = Constants.COLOR_WHITE
//    self.btnActHalfRight.backgroundColor = Constants.COLOR_PINK
//    self.btnInputSend.backgroundColor = Constants.COLOR_TRANSPARENT
//    self.btnActOfferLeft.backgroundColor = Constants.COLOR_WHITE
//    self.btnActOfferRight.backgroundColor = Constants.COLOR_PINK
    
    self.txtvwChatInput.delegate = self
    self.txtvwChatInput.stringPlaceHolder = "Type a message"
    self.txtvwChatInput.textContainerInset = UIEdgeInsetsMake(10, 0, 10, 5)
    self.txtvwChatInput.floatTopPlaceholderOriginY = 10
    self.txtvwChatInput.floatDefaultHeight = 17 + self.txtvwChatInput.textContainerInset.bottom + self.txtvwChatInput.textContainerInset.top + 3
    
//    self.txtInputOfferPrice.userInteractionEnabled = true
//    self.txtInputOfferPrice.font = Constants.FONT_LATO_HEAVY_20
//    self.txtInputOfferPrice.textColor = Utils.setHexColor("333333")
//    self.txtInputOfferPrice.backgroundColor = Constants.COLOR_WHITE
//    self.txtInputOfferPrice.delegate = self
    
    //
//    let attrStringTitleOffer = NSMutableAttributedString(string: self.lblTitleInputOffer.text!, attributes: [NSForegroundColorAttributeName: Utils.setHexColor("333333"), NSFontAttributeName : Constants.FONT_LATO_HEAVY_12!, NSKernAttributeName: 1.2])
//    self.lblTitleInputOffer.attributedText = attrStringTitleOffer
    
//    let paragraphStyle = NSMutableParagraphStyle()
//    paragraphStyle.minimumLineHeight = 15
//    paragraphStyle.alignment = NSTextAlignment.Center
//    let attrStringSellingPrice = NSMutableAttributedString(string: self.lblSellingPrice.text!, attributes: [NSForegroundColorAttributeName: Utils.setHexColor("888888"), NSFontAttributeName : Constants.FONT_LATO_REGULAR_11!, NSParagraphStyleAttributeName : paragraphStyle])
//    self.lblSellingPrice.attributedText = attrStringSellingPrice
    
    if self.isSeller {
    
      self.consTxtChatInputLeadToImgCameraTrail.priority = 1000
      self.consTxtChatInputLeadToVwContainerInputLead.priority = 888
      self.consTxtChatInputLeadToVwContainerInputLead.active = false
      self.consTxtChatInputLeadToImgCameraTrail.active = true
      
    } else {
      
      self.consTxtChatInputLeadToVwContainerInputLead.priority = 1000
      self.consTxtChatInputLeadToVwContainerInputLead.active = true
      self.consTxtChatInputLeadToImgCameraTrail.priority = 888
      self.consTxtChatInputLeadToImgCameraTrail.active = false
    }
   
    self.tblChat.delegate = self
    self.tblChat.dataSource = self
    self.tblChat.separatorStyle = UITableViewCellSeparatorStyle.None
    self.tblChat.contentInset.top = self.vwHeaderProduct.frame.size.height
    self.tblChat.scrollIndicatorInsets.top = self.vwHeaderProduct.frame.size.height
    
    self.scrollView.floatMaxOffset = 150//self.imgProduct.frame.size.height + 1
    self.scrollView.bounces = false
    self.tblChat.bounces = false
    
//    self.btnActOfferLeft.setTitle(TXT_CANCEL, forState: UIControlState.Normal)
//    self.btnActOfferRight.setTitle(TXT_SUBMIT, forState: UIControlState.Normal)
    
    self.btnActHalfLeft.setTitle("Withdraw Offer", forState: UIControlState.Normal)
    self.btnActHalfRight.setTitle("Edit Offer", forState: UIControlState.Normal)
    
  }
  
  func setDisableActionOffer(isDisable:Bool) {
    if isDisable {
      //self.btnActOfferRight.backgroundColor = Utils.setHexColor("888888")
      self.btnActOfferRight.enabled = false
    } else {
      //self.btnActOfferRight.backgroundColor = Constants.COLOR_PINK
      self.btnActOfferRight.enabled = true
    }
  }
  
  func tableViewScrollToBottom(animated: Bool) {
    
    let delay = 0.5 * Double(NSEC_PER_SEC)
    let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
    let numberOfSections = self.tblChat.numberOfSections
    let numberOfRows = self.tblChat.numberOfRowsInSection(numberOfSections-1)
    
    if numberOfRows > 0 {
       let indexPath = NSIndexPath(forRow: numberOfRows-1, inSection: (numberOfSections-1))
       self.tblChat.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Middle, animated: animated)
      dispatch_after(time, dispatch_get_main_queue(), {

        self.tblChat.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Middle, animated: animated)
        
      })
    }
    
  }
  
  //MARK: - Observer Methods
  func addObserverKeyboard() {
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ConversationView.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ConversationView.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
  }
  
  func removeObserverKeyboard() {
    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
   
  }
  
  func keyboardWillHide(notification:NSNotification) {
    if self.isKeyboardShow {
      self.isKeyboardShow = false
    }
    if !isKeyboardOffer {
      
      self.vwActionButtons.hidden = false
      self.consVwActionButtonsHeight.constant = 49
      self.consVwContainerInputBotToSuperviewBot.constant = 0
      //self.consTblChatHeight.constant = self.vwActionButtons.frame.origin.y - self.vwHeaderProduct.frame.size.height
      self.vwActionButtons.alpha = 0
      
      UIView.animateWithDuration(0.5, animations: {
        
        self.vwActionButtons.alpha = 1
        self.layoutIfNeeded()
        //self.consTblChatHeight.constant = self.vwActionButtons.frame.origin.y - self.vwHeaderProduct.frame.size.height + self.imgProduct.frame.size.height
      },completion: { (finished:Bool) -> Void in
       
        self.layoutIfNeeded()
      })
      
    } else {
      
      self.consVwContainerParentInputOfferBotToSuperviewTop.constant = self.frame.size.height
      
      UIView.animateWithDuration(0.1, animations: {
        
        self.layoutIfNeeded()
        
      },completion: { (finished:Bool) -> Void in
        
          self.isKeyboardOffer = false
      })
    }
  }
  
  func keyboardWillShow(notification:NSNotification){
    
    let dictKeyboard: NSDictionary = notification.userInfo! as NSDictionary
    let rectKeyboardEndPos:CGRect = (dictKeyboard.objectForKey(UIKeyboardFrameEndUserInfoKey) as! NSValue).CGRectValue()
    let keyboardViewEndFrame = convertRect(rectKeyboardEndPos, fromView: self.window)
    
    if !self.isKeyboardOffer {
      self.vwActionButtons.alpha = 0
      self.consVwContainerInputBotToSuperviewBot.constant = -keyboardViewEndFrame.size.height
      
      UIView.animateWithDuration(0.5, animations: {
        self.vwActionButtons.alpha = 0
        self.consVwActionButtonsHeight.constant = 0
        self.layoutIfNeeded()
          
      },completion: { (finished:Bool) -> Void in
        
          self.vwActionButtons.hidden = true
          self.tableViewScrollToBottom(true)
      })
      
    } else {
      
      self.consVwContainerParentInputOfferBotToSuperviewTop.constant = -keyboardViewEndFrame.size.height
      
      UIView.animateWithDuration(0.1, animations: {
 
        self.layoutIfNeeded()
       
      },completion: { (finished:Bool) -> Void in
        
      })
    }
  }
  
  func dismissKeyboard(){
    self.endEditing(true)
  }

  //MARK: - Data Methods
  
  func setDataToHeaderView(productDetail:String) {
    
//    if let _productDetail:ProductDetail = productDetail {
//      
//      if let _strTitleProduct:String = _productDetail.strTitle {
//        self.lblBrandTitle.text = _strTitleProduct
//      }
//      
//      if let _strBrandName:String = _productDetail.brand?.strName {
//        self.lblBrand.text = _strBrandName.uppercaseString
//      }
//      
//      if let _strPriceOri:String = _productDetail.strOriginalPrice {
//        
//        let strOriginalPriceFormated: String = Utils.formatToDecimalString(_strPriceOri, withCountryCode: Utils.getAppDelegate().userMy.strCountryCode!)
//       
//        if _strPriceOri == "0" {
//          
//          self.lblPriceOri.hidden = true
//          self.consLblPriceLeadToImgProductTrail.priority = 750
//          self.consLblPriceLeadToLblPriceOriTrail.priority = 700
//          
//        } else {
//          
//          self.lblPriceOri.hidden = false
//          self.lblPriceOri.attributedText = Utils.setStringStrikeThrough(strOriginalPriceFormated, font: self.lblPriceOri.font)
//          self.consLblPriceLeadToImgProductTrail.priority = 700
//          self.consLblPriceLeadToLblPriceOriTrail.priority = 750
//        }
//      }
//      
//      if let jArrImage = _productDetail.jArrImage {
//        if jArrImage.count > 0 {
//          let strBaseImgUrl = URLS.URL_BASE_IMAGE
//          let urlPhoto = NSURL(string: strBaseImgUrl + String(jArrImage[0]))
//        
//          self.imgProduct.contentMode = UIViewContentMode.ScaleAspectFill
//          self.imgProduct.clipsToBounds = true
//          self.imgProduct.af_setImageWithURL(
//            urlPhoto!,
//            placeholderImage: Constants.IMAGE_PLACEHOLDER_PRODUCT,
//            imageTransition: UIImageView.ImageTransition.CrossDissolve(0.5)
//          )
//        }
//      } else {
//        self.imgProduct.image = Constants.IMAGE_PLACEHOLDER_PRODUCT
//      }
//      
//      if let _strPrice:String = _productDetail.strSellingPrice {
//        
//        self.lblPrice.text = Utils.formatToDecimalString(_strPrice, withCountryCode: Utils.getAppDelegate().userMy.strCountryCode!)
//        
//        let paragraphStyle = NSMutableParagraphStyle()
//        paragraphStyle.minimumLineHeight = 15
//        paragraphStyle.alignment = NSTextAlignment.Center
//        let stringSellingPrice: String = String(format: "Selling Price: %@", self.lblPrice.text!)
//        let attrStringSellingPrice = NSMutableAttributedString(string: stringSellingPrice, attributes: [NSForegroundColorAttributeName: Utils.setHexColor("888888"), NSFontAttributeName : Constants.FONT_LATO_REGULAR_11!, NSParagraphStyleAttributeName : paragraphStyle])
//        self.lblSellingPrice.attributedText = attrStringSellingPrice
//      }
//      
//      self.txtInputOfferPrice.text = Utils.formatToDecimalString("0", withCountryCode: Utils.getAppDelegate().userMy.strCountryCode!)
//    }
    
    self.layoutIfNeeded()
  }
  
//  func setDataToArrayChat(dataConversationMsg:ConversationMessage) {
//    
//    if let _dataConversationMsg:ConversationMessage = dataConversationMsg {
//      self.arrayOfChats.append(_dataConversationMsg)
//    }
//  }
  
//  func setDataMessageConversation() -> ConversationMessage {
//    
//    let user = Utils.getAppDelegate().userMy
//    let _conversationMsg:ConversationMessage = ConversationMessage()
//    _conversationMsg.strMessage = "[conversation_detail:message]"
//    
//    let date: NSDate = NSDate()
//    var strDate: String = "0:0"
//    let dtFormatter = NSDateFormatter()
//    dtFormatter.dateFormat = " HH:mm "
//    
//    strDate = Utils.convertTimerIntervalToString(date.timeIntervalSince1970)
//
//    _conversationMsg.strCreated = strDate
//    _conversationMsg.user = user
//    
//    _conversationMsg.objMessageToken = ["conversation_detail:message":self.txtvwChatInput.text]
//    return _conversationMsg
//  }
  //MARK: - Action Methods
  
  @IBAction func doTapBtnActSingle(sender: AnyObject) {
    if  sender as? UIButton  == self.btnActSingle {
      self.txtInputOfferPrice.becomeFirstResponder()
      self.isKeyboardOffer = true
    }
  }
  
  @IBAction func doTapBtnActHalfLeft(sender: AnyObject) {
    if  sender as! UIButton  == self.btnActOfferLeft {
      self.dismissKeyboard()
    } else if sender as! UIButton  == self.btnActHalfLeft {
      // Withdraw
      
    }
  }
  
  @IBAction func doTapBtnActHalfRight(sender: AnyObject) {
    
    if  sender as! UIButton  == self.btnActHalfRight {
      
      self.vwContainerParentInputOffer.hidden = false
      self.isKeyboardOffer = true
      self.txtInputOfferPrice.becomeFirstResponder()

    } else if sender as! UIButton  == self.btnActOfferRight {
     
      // submit Offer
      
      let stringArrayOffer = self.txtInputOfferPrice.text!.componentsSeparatedByCharactersInSet(
        NSCharacterSet.decimalDigitCharacterSet().invertedSet)
      let strOfferNumberOnly: String = stringArrayOffer.joinWithSeparator("")
      self.delConversationView?.delegateSubmitOffer(strOfferNumberOnly)
      self.dismissKeyboard()
    }
  }
  
  @IBAction func txtInputOfferChanged(sender: AnyObject) {
    
  }
  
  @IBAction func doTapBtnSendChat(sender: AnyObject) {
    
    if self.txtvwChatInput.text.characters.count > 0 {
     // self.delConversationView?.delegateDoSendChat(self.setDataMessageConversation(), strMsg: self.txtvwChatInput.text)
      self.consTxtVwChatInputHeight.constant = self.txtvwChatInput.floatDefaultHeight!
      self.lblInputCounter.hidden = true
      self.txtvwChatInput.contentOffset = CGPointZero
      self.txtvwChatInput.text = ""
    }
  }
  
}

//extension ConversationView: DelegateHeaderView {
//  
//  func delegateGoBack() {
//    self.delConversationView?.delegateGoback()
//  }
//}

extension ConversationView: UITextViewDelegate {
  
  func textViewDidChange(textView: UITextView) {
    let strChar = String(textView.text.characters.count)
    let attrError = [
      NSForegroundColorAttributeName: UIColor.redColor()
       ]//NSFontAttributeName: UIFont(name: Constants.FONT_LATO_HEAVY, size: 15.0)!
    
    let attrDefault = [
      NSForegroundColorAttributeName: UIColor.blueColor()//Utils.setHexColor(Constants.COLOR_HEX_PINK)
      ]// NSFontAttributeName: UIFont(name: Constants.FONT_LATO_HEAVY, size: 15.0)!
    
    self.lblInputCounter.text = String(format:"%d/200",textView.text.characters.count)
    
    if self.txtvwChatInput.text.characters.count > 200 {
      
      let attrText = NSMutableAttributedString(string: self.lblInputCounter.text!, attributes: attrDefault)
      let charCountRange = NSMakeRange(0, strChar.characters.count)
      attrText.addAttributes(attrError, range: charCountRange)
      self.lblInputCounter.attributedText = attrText
     
      self.btnInputSend.enabled = false
     // self.btnInputSend.setTitleColor(Utils.setHexColor(Constants.COLOR_HEX_LIGHT_GREY), forState: .Disabled)
      
    } else {
      
      self.lblInputCounter.text = String(format:"%d/200",textView.text.characters.count)
      self.btnInputSend.enabled = true
    }
    
    //self.txtvwChatInput.scrollEnabled = false
    let numLines = self.txtvwChatInput.getTextviewNumberOfLine()
    //let numLines = Int(txtvwChatInput.text.sizeForWidth(txtvwChatInput.contentSize.width, font: txtvwChatInput.font!).height / txtvwChatInput.font!.lineHeight)
    
    if numLines > 2 {
      self.txtvwChatInput.contentSize.height = self.txtvwChatInput.getTxtviewStringHeight()
      self.txtvwChatInput.scrollEnabled = true
      let pointToScroll = CGPointMake(0, self.txtvwChatInput.contentSize.height - self.txtvwChatInput.frame.size
        .height - 10)
      
      self.txtvwChatInput.contentOffset = pointToScroll
      
    } else {
    
      self.consTxtVwChatInputHeight.constant = self.txtvwChatInput.getTxtviewStringHeight()
      self.txtvwChatInput.scrollEnabled = false
  
    }
    
    if numLines >= 3 {
      self.lblInputCounter.hidden = false
    }else {
      self.lblInputCounter.hidden = true
    }
    
    self.layoutIfNeeded()
  }
  
  func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
    
    return true
  }
  
  func textViewDidBeginEditing(textView: UITextView) {
    
  }
  
  func textViewDidEndEditing(textView: UITextView) {
  
  }
}

extension ConversationView: UITableViewDelegate {
  func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return 127.0
  }
  
}

extension ConversationView: UITableViewDataSource {
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if arrayOfChats.count > 0 {
      return 100//(arrayOfChats.count)
    } else {
      return 100
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCellWithIdentifier("DefaultCell")!
    
    cell.textLabel?.text = "Section \(indexPath.section) Row \(indexPath.row)"
   
//    let cell = tableView.dequeueReusableCellWithIdentifier("DefaultCell")!
//    
//    if arrayOfChats.count > 0 {
//      if let _dataConversationCell:ConversationMessage = arrayOfChats[indexPath.row] {
//        
//        let userSelf = Utils.getAppDelegate().userMy
//        
//        if _dataConversationCell.userLastSender?.strId == Constants.SHARED_ATELIER_ID {
//          
//          let cellSystem = tableView.dequeueReusableCellWithIdentifier("ConversationCellSystem", forIndexPath: indexPath) as!ConversationCellSystem
//          // cellSystem.setDummyData()
//          return cellSystem
//          
//        } else {
//          
//          if _dataConversationCell.user?.strId  == userSelf.strId {
//            
//            let cellRight = tableView.dequeueReusableCellWithIdentifier("ConversationCellRight", forIndexPath: indexPath) as! ConversationCellRight
//            cellRight.setDataCell(_dataConversationCell)
//            return cellRight
//            
//          } else {
//            
//            let cellLeft = tableView.dequeueReusableCellWithIdentifier("ConversationCellLeft", forIndexPath: indexPath) as! ConversationCellLeft
//            cellLeft.setDataCell(_dataConversationCell)
//            return cellLeft
//            
//          }
//        }
//      }
//    }
    return cell
  }
}

extension ConversationView: UITextFieldDelegate {
  
  func textFieldDidBeginEditing(textField: UITextField) {
    self.setDisableActionOffer(true)
  }
  
  func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    if range.location >= Utils.getCurrencySymbol(CountryCode: "US").characters.count {
      if range.location == Utils.getCurrencySymbol(CountryCode: "US").characters.count {
       self.setDisableActionOffer(true)
      } else {
        self.setDisableActionOffer(false)
      }
      
      let stringArrayOffer = self.txtInputOfferPrice.text!.componentsSeparatedByCharactersInSet(
        NSCharacterSet.decimalDigitCharacterSet().invertedSet)
      let strOfferNumberOnly: String = stringArrayOffer.joinWithSeparator("")
      let intOffer: Int = NSString(string: strOfferNumberOnly).integerValue
      
      if intOffer > 0 {
        self.setDisableActionOffer(false)
      } else {
        
        self.setDisableActionOffer(true)
      }

      return true
    }else {
      self.setDisableActionOffer(true)
      return false
    }
  }
}
