//
//  HomeView.swift
//  trining_swift
//
//  Created by Lucy on 4/23/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit

@IBDesignable class HomeView: UIView {
  
  @IBOutlet weak var tblSample: UITableView!
  @IBOutlet weak var scrollView: UIScrollviewCustom!
  @IBOutlet weak var vwContainerTop: UIView!
  @IBOutlet weak var lblTItle: UILabel!
  
  private var vwHome:UIView!
  private var vwContainer: UIView = {
    let view = UIView()
    view.frame = CGRect(x: 0, y: 64, width: 320, height: 180)
    view.backgroundColor = UIColor.redColor()
    return view
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
  }
  
  //MARK: INITALIZATION
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)!
   
    self.vwHome = self.loadViewFromNIB()
    
    if let _ = self.vwHome {
      self.addSubview(self.vwHome)
    }
  
  }
  override func awakeFromNib() {
    super.awakeFromNib()
    self.scrollView.delegate = self
    self.prepareUI()
    
    self.tblSample.registerClass(UITableViewCell.self, forCellReuseIdentifier: "DefaultCell")
  }
  
  //MARK: UI METHODS
  
  override func layoutSubviews() {
    self.superview?.layoutSubviews()
    
  }

  func prepareUI() {
    self.tblSample.delegate = self
    self.tblSample.dataSource = self
    self.scrollView.floatMaxOffset = 400
   
    self.scrollView.bounces = false
    self.tblSample.bounces = false
//    self.vwHome.addSubview(self.vwContainer)
//    self.translatesAutoresizingMaskIntoConstraints = false
//    let consVwContainerLeadToVwSuperLead = NSLayoutConstraint(item: self.vwContainer, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: self.vwHome, attribute: NSLayoutAttribute.Leading, multiplier: 1, constant: 0)
//    let consVwContainerTrailToVwSuperTrail = NSLayoutConstraint(item: self.vwContainer, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: self.vwHome, attribute: NSLayoutAttribute.Trailing, multiplier: 1, constant: 0)
//    let consVwContainerTopToVwContainerTopBot = NSLayoutConstraint(item: self.vwContainer, attribute: NSLayoutAttribute.TopMargin, relatedBy: NSLayoutRelation.Equal, toItem: self.vwContainerTop, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 0)
//    self.vwHome.addConstraints([consVwContainerLeadToVwSuperLead,consVwContainerTrailToVwSuperTrail])
//    
//    debugPrint(self.vwContainer)
//
//    let views = ["newView": newView]
//    
//    let widthConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:[newView(100)]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
//    view.addConstraints(widthConstraints)
//    
//    let heightConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[newView(100)]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
//    view.addConstraints(heightConstraints)
  }
}

extension HomeView: UITableViewDelegate {
  func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return 127.0
  }
  
}

extension HomeView: UITableViewDataSource {
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 100
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCellWithIdentifier("DefaultCell")!
    
    cell.textLabel?.text = "Section \(indexPath.section) Row \(indexPath.row)"
    return cell
  }
}

