//
//  Login.swift
//  trining_swift
//
//  Created by Lucy on 3/26/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit

@IBDesignable class LoginView: UIView {

  var view:UIView!
  
  //MARK: INIT CUSTOM VIEW
  
  override init(frame: CGRect)
  {
    super.init(frame: frame)
    self.prepareUI()
  }
  
  required init(coder aDecoder: NSCoder)
  {
    super.init(coder: aDecoder)!
    self.prepareUI()
  }
  
  func prepareUI()
  {
    self.view = self.loadViewFromNib()
    self.view.frame = bounds
    self.addSubview(view)
  }
  
  func loadViewFromNib() -> UIView
  {
    let bundle = NSBundle(forClass:self.dynamicType)
    let nib = UINib(nibName: "LoginView", bundle: bundle)
    let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
    
    return view
  }
}
