//
//  UITextviewBubble.swift
//  Closet
//
//  Created by Lucy on 5/23/16.
//  Copyright © 2016 Reebonz. All rights reserved.
//

import UIKit

class UITextviewBubble: UITextView {

  private var gestureLongPress: UILongPressGestureRecognizer!
  private var menuController:UIMenuController? = nil
  override func awakeFromNib() {
    self.gestureLongPress = UILongPressGestureRecognizer(target: self, action:#selector(self.doSelectBubble))
    self.gestureLongPress.numberOfTouchesRequired = 1
    self.addGestureRecognizer(self.gestureLongPress)
  }
  
  override func canPerformAction(action: Selector, withSender sender: AnyObject?) -> Bool {
    // is this even have any effect?
    return super.canPerformAction(action, withSender: sender)
  }
  
  override func caretRectForPosition(position: UITextPosition) -> CGRect {
    // remove selection
    return CGRectZero
  }
  
  override func selectionRectsForRange(range: UITextRange) -> [AnyObject] {
    // remove selection
    return []
  }
  
  func doSelectBubble() {
    //debugPrint("ShowMenuController")
    self.becomeFirstResponder()
    self.menuController = UIMenuController.sharedMenuController()
    
    let menuItem:UIMenuItem = UIMenuItem(title: "Copy", action: #selector(self.copyBubble))
    self.menuController?.menuItems = [menuItem]
    if !self.menuController!.menuVisible {
      var rect:CGRect = self.bounds
      rect.origin.y = self.bounds.size.height / 2
      
      self.menuController!.setTargetRect(rect, inView: self)
      self.menuController!.setMenuVisible(true, animated: true)
    }
  }
  
  func copyBubble() {
    //debugPrint("COOOPPPYY")
    let board = UIPasteboard.generalPasteboard()
    board.string = self.text
    let menu = UIMenuController.sharedMenuController()
    menu.setMenuVisible(false, animated: true)
    
  }
}
