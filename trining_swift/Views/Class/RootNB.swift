//
//  RootNB.swift
//  trining_swift
//
//  Created by Lucy on 3/28/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit

protocol RootNBDelegate:NSObjectProtocol {
   func toggleLeftPanel()
 
}


@IBDesignable class RootNB: UINavigationBar {
  
  var view:UIView!
  var delegateNB:RootNBDelegate?
  
  @IBOutlet weak var btnLeft: UIButton!
  override init(frame: CGRect) {
    super.init(frame: frame)
    self.prepareUI()
  }
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)!
    self.prepareUI()
  }
  
  func prepareUI() {
    self.view = self.loadViewFromNib()
    self.view.frame = bounds
    debugPrint(self.view)
    self.addSubview(view)
    self.view.backgroundColor = UIColor.blackColor()
  }
  
  func loadViewFromNib() -> UIView {
    let bundle = NSBundle(forClass:self.dynamicType)
    let nib = UINib(nibName: "RootNB", bundle: bundle)
    let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
    
    return view
  }

  // Only override drawRect: if you perform custom drawing.
  // An empty implementation adversely affects performance during animation.
  override func drawRect(rect: CGRect) {
    // Drawing code
   // self.backgroundColor = UIColor.purpleColor()
  }
  
  //Mark: - Action Methods
  @IBAction func doTapBtnLeft(sender: AnyObject) {
    
    self.delegateNB?.toggleLeftPanel()
  }
}
