//
//  UIScrollviewCustom.swift
//  Closet
//
//  Created by Lucy on 4/29/16.
//  Copyright © 2016 Reebonz. All rights reserved.
//

import UIKit

class UIScrollviewCustom: UIScrollView {

  class _DelegateProxy: NSObject, UIScrollViewDelegate {
    weak var _userDelegate: UIScrollViewDelegate?
    
    override func respondsToSelector(aSelector: Selector) -> Bool {
      return super.respondsToSelector(aSelector) || _userDelegate?.respondsToSelector(aSelector) == true
    }
    
    override func forwardingTargetForSelector(aSelector: Selector) -> AnyObject? {
      if _userDelegate?.respondsToSelector(aSelector) == true {
        return _userDelegate
      }
      else {
        return super.forwardingTargetForSelector(aSelector)
      }
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
      if let scrollViewOri:UIScrollviewCustom = scrollView as? UIScrollviewCustom {
        scrollViewOri.didScroll(scrollView)
        self._userDelegate?.scrollViewDidScroll?(scrollView)
      }
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
      
      if let scrollViewOri:UIScrollviewCustom = scrollView as? UIScrollviewCustom {
        scrollViewOri.removeObserverFromScrollviews()
        
      }
      //debugPrint("stop scroll")
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
      debugPrint("BEGIN DRAG")
      
    }
    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
      debugPrint("END DRAG")
      
      if let scrollViewOri:UIScrollviewCustom = scrollView as? UIScrollviewCustom {
        scrollViewOri.didEndDragging()
        
      }
    }
  }
  
  //MARK: Attributes
  
  private var _delegateProxy =  _DelegateProxy()
  var arrScrollviewObserved: [UIScrollView] = [UIScrollView]()
  private var contextScrollview: UInt8 = 1
  
  var floatMaxOffset:CGFloat = 0
  var floatMinOffset:CGFloat = 0
  var isShowingWhenScrollDown = true
  var isParentScrolling:Bool = false
  var isHeaderShow:Bool = true
  
  private var floatLastContenOffSet: CGFloat = 0
  var currentScroll:scrollDirection?
  //MARK: Enum
  enum scrollDirection:String {
    case Up
    case Down
    case Unknown
  }
  
  override var delegate:UIScrollViewDelegate? {
    get {
      return _delegateProxy._userDelegate
    }
    set {
      self._delegateProxy._userDelegate = newValue;
      
    }
  }
  
  //MARK: Initialization
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)!
    super.delegate = _delegateProxy
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    super.delegate = _delegateProxy
  }
  
  deinit {
    self.removeObserverFromScrollviews()
  }
  
  //MARK: - Forward methods
  func didScroll(scrollView:UIScrollView) {
    self.isParentScrolling = true
    if self.contentOffset.y > self.floatMaxOffset {
      self.contentOffset.y = self.floatMaxOffset
    }
    
    if self.floatLastContenOffSet > scrollView.contentOffset.y {
      self.currentScroll = scrollDirection.Up
      debugPrint("SCROLL UP")
      
    } else if self.floatLastContenOffSet < scrollView.contentOffset.y {
      
      self.currentScroll = scrollDirection.Down
      debugPrint("SCROLL DOWN")
    }
    
    self.floatLastContenOffSet = scrollView.contentOffset.y
    
    //debugPrint("CLASS SCROLLVIEW DID SCROLL : ", self.contentOffset)
  }
  
  func didBeginDragging() {
    
  }
  
  func didEndDragging() {
    
    if self.currentScroll == scrollDirection.Down {
      
      if self.isHeaderShow {
        self.doAnimateHideHeader()
      }
      
    } else if self.currentScroll == scrollDirection.Up {
      
      if !self.isHeaderShow {
        self.doAnimateShowHeader()
      }
    }
  }
  
  func doAnimateShowHeader() {
    debugPrint("animate show")
    if self.isShowingWhenScrollDown {
      UIView.animateWithDuration(0.2, animations: {
        
        self.setContentOffset(CGPointMake(0, 0), animated: false)
        
        },completion: { (finished:Bool) -> Void in
          
          self.isHeaderShow = true
      })
    }
  }
  
  func doAnimateHideHeader() {
    debugPrint("animate hide")
    if self.isShowingWhenScrollDown {
      UIView.animateWithDuration(0.2, animations: {
        
        self.setContentOffset(CGPointMake(0, self.floatMaxOffset), animated: false)
        
        },completion: { (finished:Bool) -> Void in
          
          self.isHeaderShow = false
      })
    }
  }
  
  //MARK: - Observer Methods
  
  func addObserverToScrollView(scrollView:UIScrollView) {
   
    if !self.arrScrollviewObserved.contains(scrollView) {
      self.arrScrollviewObserved.append(scrollView)
      scrollView.addObserver(self, forKeyPath:"contentOffset", options: [NSKeyValueObservingOptions.New, NSKeyValueObservingOptions.Old], context: &self.contextScrollview)
    }
  }
  
  func removeObserverFromScrollviews() {
    
    for _scrollView:UIScrollView in self.arrScrollviewObserved {
      _scrollView.removeObserver(self, forKeyPath: "contentOffset", context: &self.contextScrollview)
    }
    
    self.arrScrollviewObserved.removeAll()
  }
  
  override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
    
    //cause i did change how to add observer so seems no need to set its content offset based the child..
//        var pointNew: CGPoint = CGPointZero
//        var pointOld: CGPoint = CGPointZero
//    
//        var currentScroll:scrollDirection?
//    
//        if let _pointNew: NSValue = change![NSKeyValueChangeNewKey] as? NSValue  {
//          pointNew = _pointNew.CGPointValue()
//        }
//    
//        if let _pointOld:NSValue = change![NSKeyValueChangeOldKey] as? NSValue {
//          pointOld = _pointOld.CGPointValue()
//        }
//    
//    debugPrint("POINT NEW ",pointNew,"OLD",pointOld)
//    
//        if let scrollview = object as? UIScrollviewCustom {
//          if scrollview == self {
//    
//            self.setContentOffset(pointNew, animated: false)
//    
//            debugPrint("SELF")
//    
//          } else {
//    
//            if self.floatLastContenOffSet > pointNew.y {
//    
//              currentScroll = scrollDirection.Up
//    
//            } else if self.floatLastContenOffSet < pointNew.y {
//    
//              currentScroll = scrollDirection.Down
//    
//            }
//    
//            self.floatLastContenOffSet = pointNew.y
//    
//            let floatDiffy: CGFloat = pointOld.y - pointNew.y
//    
//            if currentScroll == scrollDirection.Down  {
//    
//              var pointSet: CGPoint = self.contentOffset
//    
//              if floatDiffy < self.floatMaxOffset {
//    
//                pointSet.y = pointSet.y - floatDiffy
//              }
//    
//              if pointSet.y < self.floatMaxOffset {
//    
//                self.setContentOffset(pointSet, animated: false)
//              }
//    
//              debugPrint("SCROLL UP")
//    
//            } else {
//    
//              var pointSet: CGPoint = self.contentOffset
//    
//              if floatDiffy < self.floatMaxOffset {
//    
//                pointSet.y = pointSet.y - floatDiffy
//              }
//    
//              if pointNew.y > 0 && pointNew.y < self.floatMaxOffset {
//    
//                self.setContentOffset(pointSet, animated: false)
//                
//              } else {
//                
//                //self.doAnimateShowHeader()
//              }
//              
//              debugPrint("SCROLL DOWN")
//            }
//      
//          }
//        }
  }
}

extension UIScrollviewCustom: UIGestureRecognizerDelegate {
  // can added observer via this though @_@ but...
  func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    
    if let scrollViewOther:UIScrollView = otherGestureRecognizer.view as? UIScrollView {
      
      let isShouldScroll:Bool = scrollViewOther != self
      if isShouldScroll {
        self.addObserverToScrollView(scrollViewOther)
      }
      
      return isShouldScroll
      
    } else {
      //debugPrint("NOOther")
      return true
    
//    if let scrollViewOther:UIScrollView = otherGestureRecognizer.view as? UIScrollView {
//  
//      self.addObserverToScrollView(scrollViewOther)
//      
//      
//      return false
//    
//    } else {
//      //debugPrint("NOOther")
//      return true
    }
  }
}