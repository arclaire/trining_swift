//
//  CustomTextView.swift
//  bahaso
//
//  Created by Lucy Permata Sari on 12/8/15.
//  Copyright © 2015 PT. Bahaso Global Prima. All rights reserved.
//

import UIKit

let kPlaceholderTextViewInsetSpan: CGFloat = 8

class CustomTextView: UITextView {
    
    // variables
  var floatTopPlaceholderOriginY:CGFloat = 12
  var floatMargin:CGFloat?
  var floatDefaultHeight:CGFloat?
  /** The string that will be put in the placeholder */
  @IBInspectable var stringPlaceHolder: NSString? { didSet { setNeedsDisplay() } }
  /** color for the placeholder text. Default is UIColor.lightGrayColor() */
  @IBInspectable var colorPlaceholder: UIColor = UIColor.lightGrayColor()
  
  /** Border color for the text view */
  @IBInspectable var borderColor: UIColor = UIColor.clearColor() {
      didSet {
          self.layer.borderColor = borderColor.CGColor
      }
  }
  /** Border color for the text view */
  @IBInspectable var borderWidth: CGFloat = 0.0 {
      didSet {
          self.layer.borderWidth = borderWidth
      }
  }
  /** Border color for the text view */
  @IBInspectable var cornerRadius: CGFloat = 6.0 {
      didSet {
          self.layer.cornerRadius = cornerRadius
          self.layer.masksToBounds = cornerRadius > 0.0
      }
  }

  // MARK: - SET
  
  /** Override normal text string */
  override var text: String! { didSet { setNeedsDisplay() } }
  
  /** Override attributed text string */
  override var attributedText: NSAttributedString! { didSet { setNeedsDisplay() } }
  
  /** Setting content inset needs a call to setNeedsDisplay() */
  override var contentInset: UIEdgeInsets { didSet { setNeedsDisplay() } }
  
  /** Setting font needs a call to setNeedsDisplay() */
  override var font: UIFont! { didSet { setNeedsDisplay() } }
  
  /** Setting text alignment needs a call to setNeedsDisplay() */
  override var textAlignment: NSTextAlignment {
      didSet {
          setNeedsDisplay()
      }
  }
  
  // MARK: - Lifecycle
  
  #if !TARGET_INTERFACE_BUILDER
  override func awakeFromNib() {
      super.awakeFromNib()
  }
  required init(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)!
      listenForTextChangedNotifications()
  }
  
  override init(frame: CGRect, textContainer: NSTextContainer?) {
      
      super.init(frame: frame, textContainer: textContainer)
      listenForTextChangedNotifications()
      
  }
  #endif
  
  deinit {
      
  }
  
  // MARK: - NOTIFICATIONS
  
  func listenForTextChangedNotifications() {
      NSNotificationCenter.defaultCenter().addObserver(self, selector: "textChangedForPlaceholderTextView:", name:UITextViewTextDidChangeNotification , object: self)
      NSNotificationCenter.defaultCenter().addObserver(self, selector: "textChangedForPlaceholderTextView:", name:UITextViewTextDidBeginEditingNotification , object: self)
  }
  
  /** willMoveToWindow will get called with a nil argument when the window is about to dissapear */
  override func willMoveToWindow(newWindow: UIWindow?) {
      
      super.willMoveToWindow(newWindow)
      
      if newWindow == nil { NSNotificationCenter.defaultCenter().removeObserver(self) }
      
      else {
          listenForTextChangedNotifications()
      }
  }
  
  
  // MARK: - METHODS
  func textChangedForPlaceholderTextView(notification: NSNotification) {
      
      //THIS CAUSE OF BUG !!!!
      //self.contentInset = UIEdgeInsetsMake(5, 10, 5, 10)

      setNeedsDisplay()
      setNeedsLayout()
  }
  
  override func drawRect(rect: CGRect) {
      super.drawRect(rect)
      
      let strings:NSString = self.text
      
      if strings.length == 0 && self.stringPlaceHolder != nil {
          var rectBase = placeholderBoundsContainedIn(self.bounds)
          rectBase.origin.y = 7
          if self.textAlignment == NSTextAlignment.Center {
              rectBase.origin.x = 0 // set place holder pos x, before to match the cursor for left align is set to 10 for align center no need
          } else if self.textAlignment == NSTextAlignment.Left {
              rectBase.origin.x = 10
              var inset:UIEdgeInsets = self.textContainerInset
              inset.left = 7
              inset.right = 7
              self.textContainerInset = inset

          }
          
          let font = self.font ?? self.typingAttributes[NSFontAttributeName] as? UIFont ?? UIFont.systemFontOfSize(UIFont.systemFontSize())
          
          self.colorPlaceholder.set()
          
          var customParagraphStyle: NSMutableParagraphStyle!
          if let defaultParagraphStyle =  typingAttributes[NSParagraphStyleAttributeName] as? NSParagraphStyle {
              
              customParagraphStyle = defaultParagraphStyle.mutableCopy() as! NSMutableParagraphStyle
              
          } else {
              
              customParagraphStyle = NSMutableParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle }
          
          customParagraphStyle.lineBreakMode = NSLineBreakMode.ByWordWrapping
          customParagraphStyle.alignment = self.textAlignment
          let attributes = [NSFontAttributeName: font, NSParagraphStyleAttributeName: customParagraphStyle.copy() as! NSParagraphStyle, NSForegroundColorAttributeName: self.colorPlaceholder]
          // draw in rect.
          
          self.stringPlaceHolder?.drawInRect(rectBase, withAttributes: attributes)
      }
  }
  
  func placeholderBoundsContainedIn(containerBounds: CGRect) -> CGRect {
      // get the base rect with content insets.
      let baseRect = UIEdgeInsetsInsetRect(containerBounds, UIEdgeInsetsMake(kPlaceholderTextViewInsetSpan, kPlaceholderTextViewInsetSpan/2.0, 0, 0))
      
      // adjust typing and selection attributes
      //if let _:AnyObject = typingAttributes {
          //if let paragraphStyle = typingAttributes[NSParagraphStyleAttributeName] as? NSParagraphStyle {
              //baseRect.offsetBy(dx: paragraphStyle.headIndent, dy: paragraphStyle.firstLineHeadIndent)
          //}
      //}
      
      return baseRect
  }

  func getTextviewNumberOfLine() -> Int {
    var intNumberOfLines = 0
    let floatWidthContainer:CGFloat = self.frame.size.width - self.textContainerInset.left - self.textContainerInset.right - 10
    
    let floatHeightString:CGFloat = heightWithConstrainedWidth(floatWidthContainer, font: self.font!, stringText: self.text)
    
    intNumberOfLines = Int(floatHeightString/self.font.lineHeight)
    //    debugPrint("floatHeightString = ", floatHeightString)
    //    debugPrint("width frame = ", floatWidthContainer)
    //    debugPrint("NumberLines = ", intNumberOfLines)
    //    debugPrint("ContentSizeHeight = ", self.contentSize.height)
    //    debugPrint("LineHeight = ",self.font.lineHeight)
    //    debugPrint("Container = ", self.textContainerInset)
    return intNumberOfLines
  }
  
  func heightWithConstrainedWidth(width: CGFloat, font: UIFont, stringText:NSString) -> CGFloat {
    let constraintRect = CGSize(width: width, height: CGFloat.max)
    
    let boundingBox = stringText.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
    
    return boundingBox.height
  }
  
  func getTxtviewStringHeight()-> CGFloat {
    var floatHeight:CGFloat = 40
    
    let floatWidthContainer:CGFloat = self.frame.size.width - self.textContainerInset.left - self.textContainerInset.right - 10
    
    let floatHeightString: CGFloat = heightWithConstrainedWidth(floatWidthContainer, font: self.font!, stringText: self.text)
    
    var floatDistance: CGFloat = 0
    
    if let _ = self.floatDefaultHeight {
      
      floatHeight = self.floatDefaultHeight!
      floatDistance = self.floatDefaultHeight! - self.font.lineHeight
      
    }
    
    floatHeight = floatDistance  + floatHeightString
    debugPrint("LineHeight = ",self.font.lineHeight)
    debugPrint("floatHeightString = ", floatHeightString)
    
    return floatHeight
  }

}
