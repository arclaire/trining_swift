//
//  UIPickerDateView.swift
//  Closet
//
//  Created by Lucy on 4/3/16.
//  Copyright © 2016 Reebonz. All rights reserved.
//

import UIKit
protocol DelegatePickerView:NSObjectProtocol {
  func delegatePickerDoTapDoneWithSelectedValue(strSelected:String)
  func delegatePickerViewChangeValue(stringValue:String)
  
}

protocol DelegatDatePickerView:NSObjectProtocol {
  func delegateDatePickerDoTapDoneWithSelectedValue(dateSelected:NSDate)
  func delegateDatePickerChangedValue(date:NSDate)
}

class UIPickerDateView: UIView {

  //MARK: Outlets
  @IBOutlet weak private var vwDatePicker: UIDatePicker!
  @IBOutlet weak private var vwPicker: UIPickerView!
  @IBOutlet weak private var vwToolbar: UIToolbar!
  @IBOutlet weak private var btnDone: UIBarButtonItem!
  
  //MARK: Attributes
  var isPickerActive = false
  
  var isDatePikcer = false
  var isShowToolbar = false
  weak var delPickerView:DelegatePickerView?
  weak var delDatePickerView:DelegatDatePickerView?
  private var arrPicker:[String]?
  private var intTotalComponent:Int = 1
  var intSelectedLastIndex:Int?
  
  //MARK: Layouts
  
  @IBOutlet weak var consVwDatePickerTopToToolbarBot: NSLayoutConstraint!
  @IBOutlet weak var consVwDatePickerTopToVwParentTop: NSLayoutConstraint!
  @IBOutlet weak var consVwDatePickerBotToVwParentBot: NSLayoutConstraint!
  @IBOutlet weak var consVwPickerBotToVwParentBot: NSLayoutConstraint!
  @IBOutlet weak var consVwPickerTopToVwParentTop: NSLayoutConstraint!
  @IBOutlet weak var consVwPickerTopToVwToolbarBot: NSLayoutConstraint!

  override init(frame:CGRect) {
    super.init(frame:frame)
    self.prepareUI()
  }
  
  required init(coder aDecoder:NSCoder) {
    super.init(coder:aDecoder)!
    self.prepareUI()
  }
  
  // MARK: - UI METHODS
  override func layoutSubviews() {
    super.layoutSubviews()
  }
  
  func prepareUI() {
   
    //self.backgroundColor = Constants.COLOR_WHITE
  }
  
  func setToolbarButtonTitle(strTitle:String) {
    self.btnDone.title = strTitle
  }
  
  func setPickerViewWithDataAray(arrayData:[String], totalComponentColumn:Int) {
    self.vwDatePicker.hidden = true
    self.vwPicker.hidden = false
    self.vwPicker.delegate = self
    self.vwPicker.dataSource = self

    self.arrPicker = arrayData
  
    if !self.isShowToolbar {
      self.consVwPickerBotToVwParentBot.priority = 251
      self.consVwDatePickerBotToVwParentBot.priority = 250
      self.consVwDatePickerTopToVwParentTop.priority = 251
      self.consVwPickerTopToVwToolbarBot.priority = 250
      self.consVwDatePickerTopToToolbarBot.priority = 250
      self.consVwDatePickerTopToVwParentTop.priority = 251
      var rect:CGRect = self.frame
      rect.size.height = self.vwPicker.frame.size.height
      self.frame = rect
      self.vwToolbar.hidden = true
    } else {
      self.vwToolbar.hidden = false
      self.consVwDatePickerTopToToolbarBot.priority = 251
      self.consVwDatePickerTopToVwParentTop.priority = 250
    }
    
    if let intSelected = self.intSelectedLastIndex {
      self.vwPicker.selectRow(intSelected, inComponent: 0, animated: true)
    } else {
      self.vwPicker.selectRow(0, inComponent: 0, animated: false)
    }
  }
  
  func setDatePickerViewTypeDate(witMinDate dateMin:NSDate?, dateMax:NSDate?) {
    self.vwDatePicker.hidden = false
    self.vwPicker.hidden = true
    if !self.isShowToolbar {
      self.consVwPickerBotToVwParentBot.priority = 250
      self.consVwDatePickerBotToVwParentBot.priority = 251
      self.consVwDatePickerTopToVwParentTop.priority = 250
      self.consVwPickerTopToVwToolbarBot.priority = 251
      self.consVwDatePickerTopToToolbarBot.priority = 251
      self.consVwDatePickerTopToVwParentTop.priority = 250
      var rect:CGRect = self.frame
      rect.size.height = self.vwDatePicker.frame.size.height
      self.frame = rect
      self.vwToolbar.hidden = true
    }
    let stringDeviceLanguange:String = NSLocale.preferredLanguages()[0]
    self.vwDatePicker.datePickerMode = UIDatePickerMode.Date
    self.vwDatePicker.timeZone = NSTimeZone(name:"GMT")
    self.vwDatePicker.locale = NSLocale(localeIdentifier: stringDeviceLanguange)
    
    if let dateMin = dateMin {
      self.vwDatePicker.minimumDate = dateMin
    }
    
    if let dateMax = dateMax {
      self.vwDatePicker.maximumDate = dateMax
    }
  }
  
  func setDatePickerViewTypeTime() {
    self.vwDatePicker.hidden = false
    self.vwPicker.hidden = true
    if !self.isShowToolbar {
      self.consVwPickerBotToVwParentBot.priority = 250
      self.consVwDatePickerBotToVwParentBot.priority = 251
      self.consVwDatePickerTopToVwParentTop.priority = 250
      self.consVwPickerTopToVwToolbarBot.priority = 251
      self.consVwDatePickerTopToToolbarBot.priority = 251
      self.consVwDatePickerTopToVwParentTop.priority = 250
      var rect:CGRect = self.frame
      rect.size.height = self.vwDatePicker.frame.size.height
      self.frame = rect
      self.vwToolbar.hidden = true
    }
    
    self.vwDatePicker.datePickerMode = UIDatePickerMode.Time
  }
  
  func showPickerViewIn(viewParent:UIView) {
    
    if !isPickerActive {
      var rect:CGRect = self.frame
      rect.origin.x = 0
      rect.origin.y = viewParent.frame.size.height
      rect.size.width = viewParent.frame.size.width
      self.frame = rect
      
      viewParent.addSubview(self)
      
      UIView.animateWithDuration(0.15, animations: { () -> Void in
        self.frame.origin.y = viewParent.frame.size.height - self.frame.size.height
        
      }) { (finished) -> Void in
        self.isPickerActive = true
        viewParent.layoutIfNeeded()
      }
    }
  }
  
  func dismissPickerViewIn(viewParent:UIView) {
    
    if self.isPickerActive {
           
      UIView.animateWithDuration(0.15, animations: { () -> Void in
        self.frame.origin.y = viewParent.frame.size.height
        
      }) { (finished) -> Void in
        self.isPickerActive = false
        self.removeFromSuperview()
      }
      viewParent.layoutIfNeeded()
    }
  }
  
  @IBAction func doTapBtnDone(sender: AnyObject) {
    
    if let _ = self.delDatePickerView {
      
      let dateValue:NSDate = self.vwDatePicker.date
      self.delDatePickerView!.delegateDatePickerDoTapDoneWithSelectedValue(dateValue)
    
    } else if let _ = self.delPickerView {
     
      if let intSelected:Int = self.vwPicker.selectedRowInComponent(0) as Int {
        // assume only 1 component
        let strSelected:String = self.arrPicker![intSelected] as String
        self.delPickerView!.delegatePickerDoTapDoneWithSelectedValue(strSelected)
      }
    }
  }
  
  @IBAction func datePickerValueChanged(sender: AnyObject) {
    let dateValue:NSDate = sender.date
    self.delDatePickerView?.delegateDatePickerChangedValue(dateValue)
  }
  
}

extension UIPickerDateView:UIPickerViewDataSource {
  func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    if let _ = self.arrPicker {
      return (self.arrPicker?.count)!
    }
    return 0
  }
  
  func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
    return intTotalComponent
  }
  
}

extension UIPickerDateView:UIPickerViewDelegate {
  func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    if let stringTitle:String = self.arrPicker![row] as String {
      return stringTitle
    }
    
    return " "
  }
  
  func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    if let stringTitle:String = self.arrPicker![row] as String {
      self.intSelectedLastIndex = row
      self.delPickerView?.delegatePickerViewChangeValue(stringTitle)
    }
  }
}
