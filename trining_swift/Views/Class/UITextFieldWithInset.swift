//
//  UITextFieldWithInset.swift
//  Closet
//
//  Created by Lucy on 4/3/16.
//  Copyright © 2016 Reebonz. All rights reserved.
//

import UIKit

class UITextFieldWithInset: UITextField {

  var insetTextfield:UIEdgeInsets!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    self.insetTextfield = UIEdgeInsetsMake(0, 15, 0, 0)
  }
  
  override func textRectForBounds(bounds: CGRect) -> CGRect {
    return self.newBounds(bounds)
  }
  
  override func placeholderRectForBounds(bounds: CGRect) -> CGRect {
    return self.newBounds(bounds)
  }
  
  override func editingRectForBounds(bounds: CGRect) -> CGRect {
    return self.newBounds(bounds)
  }
  
  private func newBounds(bounds: CGRect) -> CGRect {
    
    var newBounds = bounds
    newBounds.origin.x += insetTextfield.left
    newBounds.origin.y += insetTextfield.top
    newBounds.size.height -= insetTextfield.top + insetTextfield.bottom
    newBounds.size.width -= insetTextfield.left + insetTextfield.right
    return newBounds
  }

}
